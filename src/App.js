import React, { useState } from 'react';

import MainContainer from './components/MainContainer';
import Footer from './components/Footer';
import GlobalStyler from './components/GlobalStyler';
import { Header, MainLogo, NavigationBox } from './components/Header';

const App = () => {
  const [route, setRoute] = useState("producer");

  return (
    <div>
      <GlobalStyler />
      <Header>
        <MainLogo />
        <NavigationBox setRoute={setRoute} />
      </Header>
      <MainContainer route={route} setRoute={setRoute} />
      <Footer />
    </div>
  );
}

export default App;