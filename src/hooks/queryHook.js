import { gql } from "apollo-boost";

export const QUERY_CROPS = gql`
  query {
    queryCrops {
      id
      gName
      gPhone
      gLocation
      gType
      gCategory
      gSpecies
      gSewDate
      gHarvDate
      gSewWay
      gDistance
      gPH
      gSalt
      createdAt
      updatedAt
    }
  }
`;

export const QUERY_PRODUCTS = gql`
  query {
    queryProducts {
      id
      cid
      code

      gName
      gPhone
      gLocation
      gType
      gCategory
      gSpecies
      gSewDate
      gHarvDate
      gSewWay
      gDistance
      gPH
      gSalt

      sTemp
      sHumid
      oOxid
      oBenzo
      pDate
      pLocation
      pName
      pToastTemp
      pPressTemp
      createdAt
      updatedAt
    }
  }
`;