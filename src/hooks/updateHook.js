import { gql } from "apollo-boost";

export const UPDATE_PRODUCT = gql`
  mutation updateProduct (
    $code: String!

    $sTemp: String
    $sHumid: String 
    $oOxid: String 
    $oBenzo: String 
    $pDate: String 
    $pLocation: String 
    $pName: String 
    $pToastTemp: String 
    $pPressTemp: String
  ) {
    updateProduct (
      code: $code

      sTemp: $sTemp
      sHumid: $sHumid
      oOxid: $oOxid
      oBenzo: $oBenzo
      pDate: $pDate
      pLocation: $pLocation
      pName: $pName
      pToastTemp: $pToastTemp
      pPressTemp: $pPressTemp
    ) {
      id
      code
    }
  }
`;