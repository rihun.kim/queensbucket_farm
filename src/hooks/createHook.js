import { gql } from "apollo-boost";

export const CREATE_CROP = gql`
  mutation createCrop (
    $gName: String
    $gPhone: String
    $gLocation: String
    $gType: String
    $gCategory: String
    $gSpecies: String
    $gSewDate: String
    $gHarvDate: String
    $gSewWay: String
    $gDistance: String
    $gPH: String
    $gSalt: String
  ) {
    createCrop (
      gName: $gName
      gPhone: $gPhone
      gLocation: $gLocation
      gType: $gType
      gCategory: $gCategory
      gSpecies: $gSpecies
      gSewDate: $gSewDate
      gHarvDate: $gHarvDate
      gSewWay: $gSewWay
      gDistance: $gDistance
      gPH: $gPH
      gSalt: $gSalt
    ) {
      id
      gName
    }
  }
`;

export const CREATE_PRODUCT = gql`
  mutation createProduct (
    $cid: String
    $code: String

    $gName: String
    $gPhone: String
    $gLocation: String
    $gType: String
    $gCategory: String
    $gSpecies: String
    $gSewDate: String
    $gHarvDate: String
    $gSewWay: String
    $gDistance: String
    $gPH: String
    $gSalt: String

    $sTemp: String
    $sHumid: String
    $oOxid: String
    $oBenzo: String
    $pDate: String
    $pLocation: String
    $pName: String
    $pToastTemp: String
    $pPressTemp: String
  ) {
    createProduct (
      cid: $cid
      code: $code

      gName: $gName
      gPhone: $gPhone
      gLocation: $gLocation
      gType: $gType
      gCategory: $gCategory
      gSpecies: $gSpecies
      gSewDate: $gSewDate
      gHarvDate: $gHarvDate
      gSewWay: $gSewWay
      gDistance: $gDistance
      gPH: $gPH
      gSalt: $gSalt

      sTemp: $sTemp
      sHumid: $sHumid
      oOxid: $oOxid
      oBenzo: $oBenzo
      pDate: $pDate
      pLocation: $pLocation
      pName: $pName
      pToastTemp: $pToastTemp
      pPressTemp: $pPressTemp
    ) {
      id
      gName
    }
  }
`;