import React, { useState } from "react";

import PropTypes from "prop-types";
import styled from "styled-components";

const Viewer = styled.input`
  width: 100%;
  height: 35px;

  border: 1px solid #BCBCBC;
  box-sizing: border-box;         
  font-size: 17px;
  margin: 0 0 10px 0;  
  padding: 0 15px 0 15px;
`;

export const useInput = () => {
  const [value, setValue] = useState("");

  const onChange = e => {
    const { target: { value } } = e;
    setValue(value);
  };

  const reFresh = () => {
    setValue("");
  }

  return { onChange, reFresh, setValue, value };
};

export const Input = ({ onChange, placeholder, required, type = "text", value }) => (
  <Viewer onChange={onChange} placeholder={placeholder} required={required} type={type} value={value} />
);

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string.isRequired,
};