import React, { useState } from 'react';

import { useMutation } from "@apollo/react-hooks";
import styled from 'styled-components';

import { CREATE_CROP } from '../hooks/createHook';
import { Input, useInput } from './Input';

const Viewer = styled.div`
  width: 100%;
  height: 100%;
  
  align-items: center;
  display: flex;
  font-size: 14px;
  flex-direction: column;
  justify-content: center;
  margin: 20px 0 0 0;
`;

const Button = styled.button`
  width: 100%;
  height: 50px;

  background-color: #F5EE28;
  border: 0;
  cursor: pointer;
  color: #000000;
  font-size: 14px;
  margin: 20px 0 0 0;
  text-align: center;
`;

const FarmerContainer = ({ setRoute, setWriter }) => {
  const gName = useInput(), gPhone = useInput(), gLocation = useInput(), gType = useInput();
  const gCategory = useInput(), gSpecies = useInput(), gSewDate = useInput(), gHarvDate = useInput();
  const gSewWay = useInput(), gDistance = useInput(), gPH = useInput(), gSalt = useInput();

  const [createMutation] = useMutation(CREATE_CROP, {
    variables: {
      gName: gName.value, gPhone: gPhone.value, gLocation: gLocation.value,
      gType: gType.value, gCategory: gCategory.value, gSpecies: gSpecies.value,
      gSewDate: gSewDate.value, gHarvDate: gHarvDate.value, gSewWay: gSewWay.value,
      gDistance: gDistance.value, gPH: gPH.value, gSalt: gSalt.value
    }
  });

  const [emit, setEmit] = useState(false);
  const [buttonSign, setButtonSign] = useState('제 출 하 기');

  const onSubmit = async e => {
    e.preventDefault();

    try {
      setEmit(true);
      setButtonSign('제 출 중 입 니 다 ...');
      const { data: createdData } = await createMutation();
      setWriter(createdData.createCrop.gName)
      setRoute("done")
    } catch (error) {
      setEmit(false);
      setButtonSign('제 출 하 기');
      alert('내용이 전송되지 않았습니다. 다시 시도해주세요.');
      console.log(error);
    }
  }

  return (
    <Viewer>
      <form onSubmit={onSubmit}>
        <Input placeholder={"생산자 ::: 성명 또는 작목반 으로 적어주세요."} required={true} {...gName} />
        <Input placeholder={"휴대폰 ::: 연락 받으실 휴대폰 번호 를 적어주세요."} required={true} {...gPhone} />
        <Input placeholder={"생산지역 ::: 군/읍/면 또는 시/구/동 으로 적어주세요."} required={true} {...gLocation} />
        <Input placeholder={"작형 ::: 노지 또는 하우스 으로 적어주세요."} required={true} {...gType} />
        <Input placeholder={"품목 ::: 참깨/들깨/검은깨 등 으로 적어주세요."} required={true} {...gCategory} />
        <Input placeholder={"품종 ::: 강안/건백/상백/수지/밀성/평안/고품/백설/안산 등 으로 적어주세요."} required={true} {...gSpecies} />
        <Input placeholder={"파종일자 ::: 연 월 일 으로 적어주세요."} required={true} {...gSewDate} />
        <Input placeholder={"수확일자 ::: 연 월 일 으로 적어주세요."} required={true} {...gHarvDate} />
        <Input placeholder={"파종방식 ::: 직파/모종/파종 등 으로 적어주세요."} required={true} {...gSewWay} />
        <Input placeholder={"식재간격 ::: 길이 CM 으로 적어주세요."} required={true} {...gDistance} />
        <Input placeholder={"토양산도 ::: pH 으로 적어주세요."} required={true} {...gPH} />
        <Input placeholder={"토양염도 ::: dS/m 또는 cS/m 으로 적어주세요."} required={true} {...gSalt} />
        <Button disabled={emit}>{buttonSign}</Button>
      </form>
    </Viewer>
  );
}

export default FarmerContainer;