import React, { useState } from 'react';

import { useQuery } from "@apollo/react-hooks";
import styled from 'styled-components';

import { CropsModal, CropsTable } from './CropContainer';
import { ProductsModal, ProductsTable } from './ProductContainer';
import Spinner from './Spinner';
import { QUERY_CROPS, QUERY_PRODUCTS } from '../hooks/queryHook';

const Viewer = styled.div`
  width: 100%;
  height: 100%;
  
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 20px 0 0 0;
  &:last-child {
    margin: 150px 0 0 0;
  }
`;

const ProducerContainer = () => {
  const { data: remoteCropsData } = useQuery(QUERY_CROPS);
  const { data: remoteProductsData } = useQuery(QUERY_PRODUCTS);

  const [cropId, setCropId] = useState("");
  const [productCode, setProductCode] = useState("");
  const [cropModalOn, setCropModalOn] = useState(false);
  const [productModalOn, setProductModalOn] = useState(false);

  const toggleCropModal = async ({ id }) => {
    setCropId(id);
    cropModalOn === true ? setCropModalOn(false) : setCropModalOn(true)
  }

  const toggleProductModal = async ({ code }) => {
    setProductCode(code);
    productModalOn === true ? setProductModalOn(false) : setProductModalOn(true)
  }

  if (remoteCropsData === undefined || remoteProductsData === undefined) {
    return (<Spinner />);
  } else {
    let localCropsData = remoteCropsData.queryCrops;
    let localProductsData = remoteProductsData.queryProducts;

    return (
      <div>
        <Viewer>
          <CropsTable localCropsData={localCropsData} toggleModal={toggleCropModal} />
          <CropsModal
            cropId={cropId} cropModalOn={cropModalOn}
            localCropsData={localCropsData} localProductsData={localProductsData}
            toggleModal={toggleCropModal} />
        </Viewer>
        <Viewer>
          <ProductsTable localProductsData={localProductsData} toggleModal={toggleProductModal} />
          <ProductsModal
            localProductsData={localProductsData}
            productCode={productCode} productModalOn={productModalOn}
            toggleModal={toggleProductModal} />
        </Viewer>
      </div>
    );
  }
}

export default ProducerContainer;