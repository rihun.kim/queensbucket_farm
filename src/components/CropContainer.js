import React from 'react';
import Modal from 'react-modal';

import { useMutation } from "@apollo/react-hooks";
import styled from 'styled-components';

import { CREATE_PRODUCT } from '../hooks/createHook';
import { generateCode } from '../resources/Words';

Modal.setAppElement(document.getElementById('root'))

const Table = styled.table`
  width: 100%;
`

const Thead = styled.thead`
  background-color: #F5EE28;  
  font-size: 13px;
`

const TBody = styled.tbody`
  background-color: #FFFFFF;  
  font-size: 12px;

  &:hover {
    cursor: pointer;
    background-color: #9BFFF6;
  }
`

const Tr = styled.tr`
`

const Th = styled.th`
  text-align: center;
  padding: 10px 0 10px 0;
`

const Tdc = styled.td`
  text-align: center;
  padding: 10px 0 10px 0;
`

const Button = styled.button`
  width: 100%;
  height: 50px;

  background-color: #F5EE28;
  border: 0;
  cursor: pointer;
  color: #000000;
  font-size: 14px;
  margin: 20px 0 0 0;
  text-align: center;
`;

const CropModalStyle = {
  content: {
    width: '300px',
    height: '160px',

    border: '1px solid rgb(236, 236, 236)',
    display: 'column',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '9999',
  }
};

export const CropsTable = ({ localCropsData, toggleModal }) => {
  const convertTime = (createdAt) => {
    const time = new Date(createdAt);
    const convertedTime =
      "" + time.getFullYear() + "." + (time.getMonth() + 1) + "." + time.getDate() +
      " / " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();

    return convertedTime;
  }

  return (
    <Table>
      <Thead>
        <Tr>
          <Th>제출일</Th><Th>생산자 / 휴대폰 번호</Th><Th>지역 / 작형</Th><Th>품목 / 품종</Th>
          <Th>파종 / 수확일</Th><Th>파종방식 / 식재간격</Th><Th>토양산도 / 염도</Th>
        </Tr>
      </Thead>
      {localCropsData.map((
        { id, createdAt, gName, gPhone, gLocation, gType, gCategory, gSpecies,
          gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt }) => (
          <TBody key={id}>
            <Tr onClick={() => toggleModal({ id })}>
              <Tdc>{convertTime(createdAt)}</Tdc><Tdc>{gName} / {gPhone}</Tdc>
              <Tdc>{gLocation} / {gType}</Tdc><Tdc>{gCategory} / {gSpecies}</Tdc>
              <Tdc>{gSewDate} / {gHarvDate}</Tdc><Tdc>{gSewWay} / {gDistance}</Tdc>
              <Tdc>{gPH} / {gSalt}</Tdc>
            </Tr>
          </TBody>))}
    </Table>
  );
}

export const CropsModal = ({ cropId, cropModalOn, localCropsData, localProductsData, toggleModal }) => {
  const [createProductMutation] = useMutation(CREATE_PRODUCT);

  const addLocalProductData = () => {
    let usedCodes = [], code;

    for (let key in localProductsData) {
      usedCodes.push(localProductsData[key].code);
    }
    while (usedCodes.indexOf(code = generateCode()) === -1 ? false : true);

    for (let key in localCropsData) {
      if (cropId === localCropsData[key].id) {
        const productData = Object.assign({}, localCropsData[key]);
        productData.cid = productData.id; productData.id = "";
        productData.code = code; productData.sTemp = ""; productData.sHumid = "";
        productData.oOxid = ""; productData.oBenzo = ""; productData.pDate = "";
        productData.pLocation = ""; productData.pName = ""; productData.pToastTemp = "";
        productData.pPressTemp = "";

        localProductsData.push(productData);
        return productData;
      }
    }
  }

  const addRemoteProductData = async e => {
    e.preventDefault();
    try {
      const productData = addLocalProductData();
      createProductMutation({
        variables: {
          cid: productData.cid, gName: productData.gName, gPhone: productData.gPhone, gLocation: productData.gLocation,
          gType: productData.gType, gCategory: productData.gCategory, gSpecies: productData.gSpecies,
          gSewDate: productData.gSewDate, gHarvDate: productData.gHarvDate, gSewWay: productData.gSewWay,
          gDistance: productData.gDistance, gPH: productData.gPH, gSalt: productData.gSalt, code: productData.code
        }
      });
      toggleModal("");
    } catch (error) {
      console.log(error);
      alert(`내용이 저장되지 않았습니다. 다시 시도해주세요.`);
    }
  }

  return (
    <Modal isOpen={cropModalOn} style={CropModalStyle}>
      <form onSubmit={addRemoteProductData}>
        <Button>생 성 하 기</Button>
      </form>
      <Button onClick={() => toggleModal("")}>취 소 하 기</Button>
    </Modal>
  );
}