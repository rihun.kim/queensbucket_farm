import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body {
    font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Oxygen,Ubuntu,Cantarell,'Open Sans','Helvetica Neue',sans-serif;
    max-width: 1200px;
    margin: 0 auto 0 auto;
    padding: 10px 10px 0 10px;
  }
`;