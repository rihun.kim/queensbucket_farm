import React, { useState } from 'react';

import Confirmer from './Confirmer';
import FarmerContainer from './FarmerContainer';
import ProducerContainer from './ProducerContainer';

const MainContainer = ({ route, setRoute }) => {
  const [writer, setWriter] = useState("");

  if (route === "farmer") {
    return (<FarmerContainer setRoute={setRoute} setWriter={setWriter} />);
  } else if (route === "producer") {
    return (
      //prompt("producer 비밀번호를 입력하세요.") === "queensbucket" ? (<ProducerContainer />) : (<div></div>)
      <ProducerContainer />
    );
  } else if (route === "done") {
    return (<Confirmer writer={writer} />);
  }
}

export default MainContainer;