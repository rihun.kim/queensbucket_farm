import React from 'react';

import styled from 'styled-components';

const Viewer = styled.div`
  width: 100%;
  height: 100%;
  
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 20px 0 0 0;
`;

const Confirmer = (writer) => {
  const { writer: farmer } = writer;

  return (
    <Viewer>
      {farmer} 님의 내용이 잘 전송되었습니다.
    </Viewer>
  );
}

export default Confirmer;