import React from 'react';
import Modal from 'react-modal';

import { useMutation } from "@apollo/react-hooks";
import styled from 'styled-components';

import { UPDATE_PRODUCT } from '../hooks/updateHook';

import { Input, useInput } from './Input';

const Table = styled.table`
  width: 100%;
`

const Thead = styled.thead`
  background-color: #9FF781;  
  font-size: 13px;
`

const TBody = styled.tbody`
  background-color: #FFFFFF;  
  font-size: 12px;
  
  &:hover {
    cursor: pointer;
    background-color: #9BFFF6;
  }
`

const Tr = styled.tr`
`

const Th = styled.th`
  text-align: center;
  padding: 10px 0 10px 0;
`

const Tdc = styled.td`
  text-align: center;
  padding: 10px 0 10px 0;
`

const Button = styled.button`
  width: 100%;
  height: 50px;

  background-color: #F5EE28;
  border: 0;
  cursor: pointer;
  color: #000000;
  font-size: 14px;
  margin: 10px 0 0 0;
  text-align: center;
`;

const Title = styled.div`
  width: 100%;
  height: 14px;

  color: #000000;
  font-size: 14px;
  margin: 0 0 20px 0;
  text-align: center;
`;

const ProductModalStyle = {
  content: {
    width: '300px',
    height: '560px',

    border: '1px solid rgb(236, 236, 236)',
    display: 'column',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '9999',
  }
};

export const ProductsTable = ({ localProductsData, toggleModal }) => {
  const convertTime = (createdAt) => {
    const time = new Date(createdAt);
    const convertedTime =
      "" + time.getFullYear() + "." + (time.getMonth() + 1) + "." + time.getDate() +
      " / " + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();

    return convertedTime;
  }

  return (
    <Table>
      <Thead>
        <Tr>
          <Th>제출일 / 생산자 / 휴대폰 번호 / 지역 / 작형 / 품목 / 품종 / 파종 / 수확일 / 파종방식 / 식재간격 / 토양산도 / 염도</Th>
          <Th>코드</Th><Th>온도</Th><Th>습도</Th><Th>산가</Th><Th>벤조피렌</Th>
          <Th>제조일자</Th><Th>제조공장</Th><Th>담당자</Th>
          <Th>볶음온도상한</Th><Th>착유온도</Th>
        </Tr>
      </Thead>
      {localProductsData.map((
        { createdAt, gName, gPhone, gLocation, gType, gCategory, gSpecies,
          gSewDate, gHarvDate, gSewWay, gDistance, gPH, gSalt, code,
          sTemp, sHumid, oOxid, oBenzo, pDate, pLocation, pName, pToastTemp, pPressTemp }) => (
          <TBody key={code}>
            <Tr onClick={() => toggleModal({ code })}>
              <Tdc>{convertTime(createdAt)} / {gName} / {gPhone} / {gLocation} / {gType}
                / {gCategory} / {gSpecies} / {gSewDate} / {gHarvDate} / {gSewWay} /
                    {gDistance} / {gPH} / {gSalt}</Tdc>
              <Tdc>{code}</Tdc><Tdc>{sTemp}</Tdc><Tdc>{sHumid}</Tdc><Tdc>{oOxid}</Tdc>
              <Tdc>{oBenzo}</Tdc><Tdc>{pDate}</Tdc><Tdc>{pLocation}</Tdc><Tdc>{pName}</Tdc>
              <Tdc>{pToastTemp}</Tdc><Tdc>{pPressTemp}</Tdc>
            </Tr>
          </TBody>))}
    </Table>
  );
}

export const ProductsModal = ({ localProductsData, productCode, productModalOn, toggleModal }) => {
  const [updateProductMutation] = useMutation(UPDATE_PRODUCT);

  const sTemp = useInput(), sHumid = useInput(), oOxid = useInput(), oBenzo = useInput();
  const pDate = useInput(), pLocation = useInput(), pName = useInput(), pToastTemp = useInput(), pPressTemp = useInput();

  const getPlaceholder = name => {
    for (let key in localProductsData) {
      if (productCode === localProductsData[key].code) {
        if (name === "sTemp") {
          return "온도 : " + localProductsData[key].sTemp;
        } else if (name === "sHumid") {
          return "습도 : " + localProductsData[key].sHumid;
        } else if (name === "oOxid") {
          return "산가 : " + localProductsData[key].oOxid;
        } else if (name === "oBenzo") {
          return "벤조피렌 : " + localProductsData[key].oBenzo;
        } else if (name === "pDate") {
          return "제조일자 : " + localProductsData[key].pDate;
        } else if (name === "pLocation") {
          return "제조공장 : " + localProductsData[key].pLocation;
        } else if (name === "pName") {
          return "담당자 : " + localProductsData[key].pName;
        } else if (name === "pToastTemp") {
          return "볶음온도상한 : " + localProductsData[key].pToastTemp;
        } else if (name === "pPressTemp") {
          return "착유온도 : " + localProductsData[key].pPressTemp;
        }
      }
    }
  }

  const refreshIput = () => {
    sTemp.reFresh();
    sHumid.reFresh();
    oOxid.reFresh();
    oBenzo.reFresh();
    pDate.reFresh();
    pLocation.reFresh();
    pName.reFresh();
    pToastTemp.reFresh();
    pPressTemp.reFresh();
    toggleModal("");
  }

  const amendLocalProductData = () => {
    for (let key in localProductsData) {
      if (productCode === localProductsData[key].code) {
        let ref = localProductsData[key];
        console.log(sTemp.value);
        ref.sTemp = sTemp.value !== "" ? sTemp.value : ref.sTemp;
        ref.sHumid = sHumid.value !== "" ? sHumid.value : ref.sHumid;
        ref.oOxid = oOxid.value !== "" ? oOxid.value : ref.oOxid;
        ref.oBenzo = oBenzo.value !== "" ? oBenzo.value : ref.oBenzo;
        ref.pDate = pDate.value !== "" ? pDate.value : ref.pDate;
        ref.pLocation = pLocation.value !== "" ? pLocation.value : ref.pLocation;
        ref.pName = pName.value !== "" ? pName.value : ref.pName;
        ref.pToastTemp = pToastTemp.value !== "" ? pToastTemp.value : ref.pToastTemp;
        ref.pPressTemp = pPressTemp.value !== "" ? pPressTemp.value : ref.pPressTemp;

        return ref;
      }
    }
  }

  const amendRemoteProductData = async e => {
    e.preventDefault();
    try {
      const productData = amendLocalProductData();
      updateProductMutation({
        variables: {
          code: productData.code, sTemp: productData.sTemp, sHumid: productData.sHumid, oOxid: productData.oOxid,
          oBenzo: productData.oBenzo, pDate: productData.pDate, pLocation: productData.pLocation, pName: productData.pName,
          pToastTemp: productData.pToastTemp, pPressTemp: productData.pPressTemp
        }
      });
      refreshIput();
    } catch (error) {
      console.log(error);
      alert(`내용이 저장되지 않았습니다. 다시 시도해주세요.`);
    }
  }

  return (
    <Modal isOpen={productModalOn} style={ProductModalStyle}>
      <form onSubmit={amendRemoteProductData}>
        <Title>[ {productCode} ]</Title>
        <Input placeholder={getPlaceholder("sTemp")} required={false} {...sTemp} />
        <Input placeholder={getPlaceholder("sHumid")} required={false} {...sHumid} />
        <Input placeholder={getPlaceholder("oOxid")} required={false} {...oOxid} />
        <Input placeholder={getPlaceholder("oBenzo")} required={false} {...oBenzo} />
        <Input placeholder={getPlaceholder("pDate")} required={false} {...pDate} />
        <Input placeholder={getPlaceholder("pLocation")} required={false} {...pLocation} />
        <Input placeholder={getPlaceholder("pName")} required={false} {...pName} />
        <Input placeholder={getPlaceholder("pToastTemp")} required={false} {...pToastTemp} />
        <Input placeholder={getPlaceholder("pPressTemp")} required={false} {...pPressTemp} />
        <Button>제 출 하 기</Button>
      </form>
      <Button onClick={() => refreshIput()}>취 소 하 기</Button>
    </Modal>
  );
}