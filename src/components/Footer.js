import React from 'react';

import styled from "styled-components";

import { ADDRESSINFOPHRASE } from "../resources/Catchphrase";

const CompanyInfo = styled.div`
  width: 100%;
  height: 50px;

  margin: 40px auto 0px auto;
`;

const AddressInfo = styled.div`
  color: #9E9E9E;
  font-size: 9px;  
`;

export default () => (
  <CompanyInfo>
    <AddressInfo>&copy;{ADDRESSINFOPHRASE}</AddressInfo>
  </CompanyInfo>
);