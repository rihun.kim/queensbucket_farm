import React from 'react';
import Loader from 'react-loader-spinner'

import styled from 'styled-components';

const Viewer = styled.div`
  width: 100%;
  height: 100%;
  
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 20px 0 0 0;
`;

const Spinner = () => (
  <Viewer><Loader type="Grid" color="#6FBA45" height={30} width={30} /></Viewer>
);

export default Spinner;