import React from 'react';

import styled from "styled-components";

import logo from '../resources/logo.png';

export const Header = styled.div`
  width: 100%;
  height: 65px;

  margin: 0 auto 0 auto;

  @media (min-width : 1050px) {
    display: flex;
    flex-direction: row;
  }
`;

const Logo = styled.div`
  width: 250px;
  height: 23px;

  margin: 0 auto 0 auto;

  @media (min-width : 1050px) {
    width: 22%;
    height: 23px;
  
    margin: auto 0 auto 0;
  }
`;

export const MainLogo = () => (
  <Logo>
    <a href=".">
      <img src={logo} alt="queensbucket grower" width="200px" height="23px" />
    </a>
  </Logo>
);

const Box = styled.div`
  width: 100%;
  height: 22px;

  margin: 10px 0 0 0;

  @media (min-width : 1050px) {
    width: 85%;
    height: 22px;

    margin: auto 0 auto 0;
  }
`;

const Link = styled.span`
  color: #4F819A;
  cursor: pointer;
  font-size: 13px;
  font-weight: 450;
  margin: 0 0 0 10px;
`;

export const NavigationBox = ({ setRoute }) => (
  <Box>
    <Link onClick={() => setRoute("farmer")}>farmer</Link>
    <Link onClick={() => setRoute("producer")}>producer</Link>
  </Box>
);